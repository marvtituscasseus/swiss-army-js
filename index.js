module.exports = function swissArmyJs( value ) {
    const _inputValue = value;

    const isSet = function() {
        return ( typeof _inputValue !== 'undefined' ) && ( _inputValue !== null );
    };

    const isFunc = function() {
        return isSet() && ( typeof _inputValue === 'function' );
    };

    const hasLength = function() {
        return isSet() && ( _inputValue.length > 0 );
    };

    const isEmailFormat = function() { 
        return /^(.+)@(.+)\.(.+)$/.test( _inputValue ); 
    };

    const isMoneyFormat = function() { 
        return /^(\d+)\.\d\d$/.test( _inputValue ); 
    };

    const isFloatFormat = function() { 
        return /^(\d+)\.(\d+)$/.test( _inputValue ); 
    };

    const isBrokenFloatFormat = function() { 
        return /^\.(\d+)$/.test( _inputValue.trim() ); 
    };

    const isDigitsFormat = function() { 
        return /^(\d+)$/.test( _inputValue ); 
    };

    const isNumericFormat = function() { 
        return ( isDigitsFormat() || isFloatFormat() ); 
    };

    const toInt = function() {
        let cleanValue = 0;

        if ( isSet() && isNumericFormat() ) {
            cleanValue = parseInt( _inputValue, 0 );
        }

        return cleanValue;
    };

    const toFloat = function() {
        let cleanValue = 0.0;

        if ( isSet() && isNumericFormat() ) {
            cleanValue = parseFloat( _inputValue );
        }

        return cleanValue;
    };

    const toCleanFloatString = function() {
        let cleanValue = '';

        if ( isBrokenFloatFormat() ) {
            cleanValue = '0' + _inputValue;
        }
        else {
            cleanValue = _inputValue;
        }

        return clean_value;
    };

    const strtr = function( tpl ) {
        let translated = '' + tpl;

        for (let tokenI in _inputValue) {
            translated = translated.replace( tokenI, _inputValue[ tokenI ] );
        }  

        return translated;
    };

    const inArray = function( arrayValue ) {
        return isSet() && ( _inputValue.indexOf( arrayValue ) !== -1 );
    };

    return {
        isSet: isSet,
        isFunc: isFunc,
        hasLength: hasLength,
        isEmailFormat: isEmailFormat,
        isMoneyFormat: isMoneyFormat,
        isFloatFormat: isFloatFormat,
        isBrokenFloatFormat: isBrokenFloatFormat,
        isDigitsFormat: isDigitsFormat,
        isNumericFormat: isNumericFormat,
        toInt: toInt,
        toFloat: toFloat,
        toCleanFloatString: toCleanFloatString,
        strtr: strtr,
        inArray: inArray,
    };
};